# 12 - Front End e Back End ?

# Desafio

Nesse desafio vocês precisarão utilizar os desafios 9, 10 ou 11 para criar um FrontEnd integrado com o BackEnd de algum desses desafios.

Vocês poderão utilizar tanto o React ou React Native, porem para os dois deve ser feito em Typescript. Aproveite a facilidade das ferramentas que criam a estrutura de pastas para utilizar uma opção que cria tudo já com Typescript, `create-react-app` por exemplo já possui essa funcionalidade.
